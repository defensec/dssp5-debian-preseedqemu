# SPDX-FileCopyrightText: © 2021 Dominick Grift <dominick.grift@defensec.nl>
# SPDX-License-Identifier: Unlicense

PREFIX ?= /usr
DESTDIR ?=
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man
BASHCOMPDIR ?= $(PREFIX)/share/bash-completion/completions

ifeq ($(WITH_BASHCOMP),)
ifneq ($(strip $(wildcard $(BASHCOMPDIR))),)
WITH_BASHCOMP := yes
endif
endif

all: clean
	argbash -o dssp5-debian-preseedqemu.sh dssp5-debian-preseedqemu.sh
	argbash --type completion --strip all -o dssp5-debian-preseedqemu.bash-completion dssp5-debian-preseedqemu.sh
	argbash --type manpage-defs --strip all -o dssp5-debian-preseedqemu-defs.rst dssp5-debian-preseedqemu.sh
	sed -i 's/<Your name goes here>/Dominick Grift <dominick.grift@defensec.nl>/' dssp5-debian-preseedqemu-defs.rst
	sed -i -e '/<More elaborate description/{r EXAMPLES' -e 'd}' dssp5-debian-preseedqemu-defs.rst
	sed -i '/[[:space:]][[:space:]][[:space:]]goes here>./d' dssp5-debian-preseedqemu-defs.rst
	argbash --type manpage --strip all -o dssp5-debian-preseedqemu.rst dssp5-debian-preseedqemu.sh
	rst2man dssp5-debian-preseedqemu.rst > dssp5-debian-preseedqemu.1

clean:
	$(RM) dssp5-debian-preseedqemu.bash-completion dssp5-debian-preseedqemu-defs.rst dssp5-debian-preseedqemu.rst dssp5-debian-preseedqemu.1

install: all
	@install -v -d "$(DESTDIR)$(MANDIR)/man1" && install -m 0644 -v dssp5-debian-preseedqemu.1 "$(DESTDIR)$(MANDIR)/man1/dssp5-debian-preseedqemu.1"
	@[ "$(WITH_BASHCOMP)" = "yes" ] || exit 0; install -v -d "$(DESTDIR)$(BASHCOMPDIR)" && install -m 0644 -v dssp5-debian-preseedqemu.bash-completion "$(DESTDIR)$(BASHCOMPDIR)/dssp5-debian-preseedqemu"
	install -v -d "$(DESTDIR)$(BINDIR)/" && install -m 0755 -v dssp5-debian-preseedqemu.sh "$(DESTDIR)$(BINDIR)/dssp5-debian-preseedqemu"

uninstall:
	@rm -vf \
		"$(DESTDIR)$(BINDIR)/dssp5-debian-preseedqemu" \
		"$(DESTDIR)$(MANDIR)/man1/dssp5-debian-preseedqemu.1" \
		"$(DESTDIR)$(BASHCOMPDIR)/dssp5-debian-preseedqemu"

.PHONY: all clean install uninstall
