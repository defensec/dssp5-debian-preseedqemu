Summary: Bash script to preseed dssp5-debian QEMU guests
Name: dssp5-debian-preseedqemu
Version: 0.1
Release: 1
License: Unlicense
Group: System Environment/Tools
Source: dssp5-debian-preseedqemu.tgz
URL: https://github.com/DefenSec/dssp5-debian-preseedqemu
Requires: qemu-img qemu-kvm-core curl
BuildRequires: argbash make python3-docutils
BuildArch: noarch

%description
Bash script to preseed dssp5-debian QEMU guests.

%prep
%autosetup -n dssp5-debian-preseedqemu

%build

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.gz
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Sat May 08 2021 Dominick Grift <dominick.grift@defensec.nl> - 0.1-1
- Initial package
